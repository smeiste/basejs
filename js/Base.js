(function () {
    //create Base
    this.Base = function () {};

    /**
     * Main function for inheritance copies 'static' (non-prototype) properties and functions as well as prototype properties
     *      and functions.  Then copies the staticProperties to the constructor function, then copies the
     *      instanceProperties to the prototype of the constructor function. Finally, constructor is returned.
     * API Notes: if the constructor that is returned has an 'init' function, this will automatically get executed.
     * @param staticProperties
     * @param instanceProperties
     * @returns {Function}
     */
    Base.extend = function (staticProperties, instanceProperties) {

        var NewClass = function (){
            if ( this.init ) {
                this.init.apply(this, arguments);
            }
        };

        //step 1: copy static functions/properties from base
        for (var stat in this) {
            if (this.hasOwnProperty(stat)) {
                NewClass[stat] = this[stat];
            }
        }

        //step 2: copy prototype from base
        for (var ins in this.prototype) {
            if (this.prototype.hasOwnProperty(ins)) {
                NewClass.prototype[ins] = this.prototype[ins];
            }
        }

        //step 3: merge static properties into new constructor
        if (staticProperties) {
            for (var statProp in staticProperties) {
                if (staticProperties.hasOwnProperty(statProp)) {
                    NewClass[statProp] = staticProperties[statProp];
                }
            }
        }

        //step 4: merge prototype
        if (instanceProperties) {
            for (var insProp in instanceProperties) {
                if (instanceProperties.hasOwnProperty(insProp)) {
                    NewClass.prototype[insProp] = instanceProperties[insProp];
                }
            }
        }

        NewClass.prototype.constructor = NewClass;

        NewClass.extend = arguments.callee;

        return NewClass;
    };
})();