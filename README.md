BaseJS
======

Simple class inhertence for JavaScript.  Keep your JavaScript neat OOP style.  BaseJS has one function: `extend()`. 
<br>
<br>
Examples:
```javascript
//declare static and prototype properties/functions to add to our extended class
var staticProps = {
        find: function () { console.log('I found one!') }
    },
    protoProps = { 
        speak: function () { console.log('Loud Noises!') },
        eat: function (food) { console.log('Yum! ' + food + '!') }
    };
    
var Animal = Base.extend( staticProps, protoProps );

var genericAnimal = new Animal();
generic.speak();    //prints 'Loud Noises' to console

//extending Animal further
var Dog = Animal.extend( {}, {
    speak: function () { console.log('Woof!') }
} );

var goldenDoodle = new Dog();
goldenDoodle.speak();   //speak got overridden, now it prints 'Woof!' to console
goldenDoodle.eat('treats');  //still prints 'Yum! treats!
```

#API

####extend( _{static properties}_, _{prototype properties}_ );
returns: new constructor function

_static properties (Object):_ properties and methods that are added directly to the constructor function.  These can be considered 'static' functions/properties and should be written in a way that they do not depend on `this` being an _instance_ of the object, but rather the constructor itself.

_prototype properties (Object):_ properties and methods that will be added to the prototype definition of the new constructor.  These will be accessible when `new` is used when creating an instance of the object.

<b>Note:</b> If your prototype properties contain a function called `init`, this function will be called automatically and passed any parameters given to the constructor.

