(function () {
    module('Testing Base "class"');

    test('Init is called', function () {
        var called = false;
        var Animal = Base.extend( {}, {init: function() {
            called = true;
        }});

        var dog = new Animal();

        ok( called );
        ok( Animal.extend );
    });

    test('static property added', function () {
        var Animal = Base.extend( {
            findOne: function () { console.log('I found one!') }
        }, {} );


        var dog = new Animal();
        ok ( !dog.findOne );

        ok( Animal.findOne );
    });

    test('instance property added', function () {
        var Animal = Base.extend( {}, {
            save: function () { console.log('saved!') }
        } );

        ok( !Animal.save );

        var dog = new Animal();

        ok( dog.save )

    });

    test('extend is added to new class', function () {
        var Animal = Base.extend();

        ok( Animal.extend );
    });

    test('test static property overload', function () {
        var Animal = Base.extend( {overloadMe: function () { return 'base overloaded'; }}),
            Dog = Animal.extend( {overloadMe: function () { return 'animal overloaded'; }} );

        equal( Dog.overloadMe(), 'animal overloaded', 'check static "overloadMe" was overloaded' );

    });

    test('test prototype property overload', function () {
        var Animal = Base.extend( {}, {overloadMe: function () { return 'base overloaded'; }}),
            Dog = Animal.extend( {}, {overloadMe: function () { return 'animal overloaded'; }}),
            goldenDoodle = new Dog();

        equal( goldenDoodle.overloadMe(), 'animal overloaded', 'check prototype "overloadMe" was overloaded' );

    });

    test('parameters passed to init via constructor', function () {
        var param1 = 'param1',
            param2 = 'param2',
            returnP1 = undefined,
            returnP2 = undefined,
            Animal = Base.extend( {}, {
                init: function (p1, p2) {
                    returnP1 = p1;
                    returnP2 = p2;
                }
            });

        var animalX = new Animal(param1, param2);

        equal( returnP1, param1, 'Check first parameter' );
        equal( returnP2, param2, 'Check second parameter' );

    });
    
})();